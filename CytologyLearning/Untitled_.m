clc, close all, clear,
addpath(genpath('../Code'));
%% generate data
opt.FixA = false; opt.Src = 4; opt.thresh = .25; opt.nVarIntens = -45; % 20; % 
opt.isVarIntens = true; opt.Sigpow = 15; opt.initA = true; opt.LargeParmtr = false; % true; % 
opt.AddPtrn = true; opt.height = 25; opt.width = 25; opt.mag = 12; 
opt.TxrPth = '../ImageTextures/'; 
opt.Txtf1 = '1.3.03.tiff'; 
opt.Txtf2 = '1.3.06.tiff'; 
opt.Txtf3 = '1.3.09.tiff'; 
opt.Txtf4 = '1.3.11.tiff';
[A_Pri, ~, X, Y, HasOverlap] = Create_Samples_(opt);
% [A_Pri, ~, X, Y, HasOverlap] = Create_Samples(opt);
while(HasOverlap ==0)
    [A_Pri, A_Pri_, X, Y, HasOverlap] = Create_Samples_(opt);
%     [A_Pri, A_Pri_, X, Y, HasOverlap] = Create_Samples_(opt);

end
img = repmat(Y,[1,1,3]);
Srcs = X;
[Xc_,Yc_,NObj] = size(Srcs);
Lbls = find_lables(Srcs);
%% Ground Truth Seg
seg(:,:,1) = (Srcs(:,:,1)==0 & Srcs(:,:,2)==0 & Srcs(:,:,3)==0 & Srcs(:,:,4)==0);
seg(:,:,2) = Srcs(:,:,1)>0;
seg(:,:,3) = Srcs(:,:,2)>0;
seg(:,:,4) = Srcs(:,:,3)>0;
seg(:,:,5) = Srcs(:,:,4)>0;
%% Learning Seg
% imageSize = [360 480 3];
% I = imresize(img,[360 480]);
% load mynet;
% newoutput = synthnet(newinput);
[~,~,C] = semanticseg(I, net);
thrsh = .25;
[Xc,Yc,~] = size(C);
res = zeros(Xc_,Yc_,NObj+1);
for ii = 1:NObj
    C_(:,:,ii) = imresize(C(:,:,ii),[Xc_,Yc_]);
end
for ii = 2:NObj+1
    res(:,:,ii) = C_(:,:,ii-1)>thrsh;
    res(:,:,1) = res(:,:,1) | res(:,:,ii);
end

%% Compare
figure,
for ii = 1:5
    subplot(3,5,ii), imshow(res(:,:,ii)), if(ii==1), title('Segmentation (Deep Learning)'), end
    subplot(3,5,ii+5), imshow(seg(:,:,ii)), if(ii==1), title('Ground Truth'), end
    if(ii<5), subplot(3,5,ii+11), imshow(Srcs(:,:,ii)), end
end
subplot(3,5,11), imshow(img), title('Mixture and Original Sources')
%%
% B = labeloverlay(I, C, 'Colormap', cmap, 'Transparency',0.4);
% figure
% imshow(B)
% pixelLabelColorbar(cmap, classes);
% % L = uint8(Lbls);
% L = imresize(L,[360 480],'nearest');