function v1 = scale2range(v1, mn, mx)
%%%%%%%%%%%%%%%%%%%%%%%%%%
% function v1 = scale2range(v1, mn, mx)
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%
 

d = mnx(  v1 );

if d(1) <0
v1 = ( v1+ abs( d(1) ) )/diff(d);
else
v1 = ( v1- abs( d(1) ) )/diff(d);
end

end