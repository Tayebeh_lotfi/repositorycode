function d=mnx( p )
% function [mn mx ]=mnx( p )
% return
% mn=min(p(:));
% mx=max(p(:));

mn=min(p(:));
mx=max(p(:));

d = [mn mx ];