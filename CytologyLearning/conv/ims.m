function ims( arg, arg2 )
% function ims( img, [scale])
%       shows image in new figure

if exist('arg2', 'var')
    imagesc( arg, arg2 )
else    
    imagesc( arg )
end
hold on;
axis image
 axis xy

impixelinfo
