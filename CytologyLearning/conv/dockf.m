function figs = dockf( n )

if nargin==0 %isempty( figs{f} )     
    n=1; 
end

if n==1
    figs =figure; set(gcf,'windowstyle','docked'); gcf;
elseif    n>2
    for f=1:n, figs{f}=figure; set(gcf,'windowstyle','docked'), gcf; end
end

%figure(figs{1});