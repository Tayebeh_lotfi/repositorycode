clc, close all, clear,
addpath(genpath('../Code'));
%% generate data

imgfolder = dir('Learn\ImgTst\*.png');
lblfiles = dir('Learn\SegTst\*.nii');
path1 = 'Learn\ImgTst\';
path2 = 'Learn\SegTst\';
thrsh = .7;
Ntrial = length(imgfolder);
% seg_cnt = zeros(Ntrial,1);
iou = zeros(1,Ntrial);
acc = zeros(1,Ntrial);
load myownnet
for iter = 1:Ntrial
    currentfilename = imgfolder(iter).name;
    X = imread([path1,currentfilename]);
    [~,~,C] = semanticseg(X, net); 
    currentfilename2 = lblfiles(iter).name;
    S = niftiread([path2,currentfilename2]);
    I = C>thrsh;   
   number = size(find(I==1),1) - size(find(I(:,:,1)==1),1);
   a = size( find(I~=S ) , 1);
   acc(iter) = 100 - a / numel(I) * 100;
   iou(iter) = 100 - a / (numel(I) - number) * 100;

end
    

%% Compare
    if(showit==1)
        figure,
        for ii = 1:5
            subplot(3,5,ii), imshow(res(:,:,ii)), if(ii==1), title('Segmentation (Deep Learning)'), end
            subplot(3,5,ii+5), imshow(seg(:,:,ii)), if(ii==1), title('Ground Truth'), end
            if(ii<5), subplot(3,5,ii+11), imshow(Srcs(:,:,ii)), end
        end
        subplot(3,5,11), imshow(img), title('Mixture and Original Sources')
    end
