from __future__ import absolute_import, division, print_function
import os
# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

# Helper libraries
import numpy as np
import matplotlib.pyplot as plt 
import scipy.io as sio
import h5py


from tensorflow.keras import models
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.layers import Activation, Reshape, Permute # keras.layers.core
from tensorflow.keras.layers import Convolution2D, MaxPooling2D, UpSampling2D # keras.layers.convolutional
from tensorflow.keras.layers import BatchNormalization

img_w = 250
img_h = 250
n_labels = 3

kernel = 3

f = h5py.File('../Code/DataGeneration/SynthDATA/Ovlpv1_/cytoimg1.mat', 'r') # ../Simulations/DataGeneration/SynthDATA/Ovlpv1_/
# mat = sio.loadmat('../Simulations/DataGeneration/SynthDATA/Ovlpv1_/cytoimage1.mat')
ls = list(f.keys())
#print('List of datasets in this file: \n',ls)
  
data=f.get('test_images')
#test_images = np.array(data)
data_ = np.array(data)
test_images = np.float32(np.transpose(data_,(0,2,3,1)))

data=f.get('test_labels')
#test_labels = np.array(data)
data_ = np.array(data)
test_labels = np.float32(np.transpose(data_,(0,2,3,1)))

data=f.get('train_images')
#train_images = np.array(data)
data_ = np.array(data)
train_images = np.float32(np.transpose(data_,(0,2,3,1)))

data=f.get('train_labels')
#train_labels = np.array(data)    
data_ = np.array(data)
train_labels = np.float32(np.transpose(data_,(0,2,3,1)))
    
f.close()


#(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()
class_names = ['ObjLayer1', 'ObjLayer2', 'ObjLayer3']

#train_images.shape
#len(train_labels)
#test_images.shape
#len(test_labels)
#
#plt.figure()
#plt.imshow(train_images[0,:,:,:])
#plt.colorbar()
#plt.grid(False)
#plt.show()
#
#plt.figure()
#plt.imshow(train_labels[0,:,:,:])
#plt.colorbar()
#plt.grid(False)
#plt.show()
#
#

model_ = [

    Convolution2D(64, kernel, padding='same', input_shape=( img_h, img_w,3),data_format="channels_last"), # border_mode='same',
    BatchNormalization(),
    Activation('relu'),
    Convolution2D(64, kernel, padding='same'),
    BatchNormalization(),
    Activation('relu'),
    MaxPooling2D(),

    Convolution2D(128, kernel, kernel, padding='same'),
    BatchNormalization(),
    Activation('relu'),
    Convolution2D(128, kernel, kernel, padding='same'),
    BatchNormalization(),
    Activation('relu'),
    MaxPooling2D(),



    UpSampling2D(),
    Convolution2D(128, kernel, kernel, padding='same'),
    BatchNormalization(),
    Activation('relu'),
    Convolution2D(64, kernel, kernel, padding='same'),
    BatchNormalization(),
    Activation('relu'),

    UpSampling2D(),
    Convolution2D(64, kernel, kernel, padding='same'),
    BatchNormalization(),
    Activation('relu'),
    Convolution2D(n_labels, 1, 1, padding='valid'),
    BatchNormalization(),
]

mymodel = models.Sequential()

for l in model_:
    mymodel.add(l)

mymodel.add(Reshape((n_labels,img_h,img_w)))
mymodel.add(Permute((2, 3, 1)))
mymodel.add(Activation('softmax'))

    
#mymodel.compile(optimizer='adam', 
#              loss='sparse_categorical_crossentropy',
#              metrics=['accuracy'])





optimizer = SGD(lr=0.001, momentum=0.9, decay=0.0005, nesterov=False)
mymodel.compile(loss="categorical_crossentropy", optimizer=optimizer)


mymodel.fit(train_images, train_labels, epochs=5) 
#
#


with open('model_5l_1.json') as model_file:
    autoencoder = models.model_from_json(model_file.read())

optimizer = SGD(lr=0.001, momentum=0.9, decay=0.0005, nesterov=False)
autoencoder.compile(loss="categorical_crossentropy", optimizer=optimizer, metrics=['accuracy'])
print ('Compiled: OK')

#train_label = train_labels[0:65,:,:,:]
#train_image = train_images[0:65,:,:,:]
#
#train_label = np.float32(train_label)
#
#train_image = np.float32(train_image)

train_images_ = train_images.astype(np.uint8)
#train_image_ = np.expand_dims(train_images_[:,:,:,0],axis=4) 
train_labels_ = train_labels

autoencoder.fit(train_images_, train_labels_, epochs=5) 


##autoencoder.fit(train_image, train_label, epochs=5) 
#autoencoder.fit(train_images_, train_labels, epochs=5) 
##train_images_ = np.expand_dims(train_images[:,:,:,0],axis=4)   
##model.fit(train_images_, train_labels, epochs=5)    



#
#
#
#with open('model_5l.json') as model_file:
#    autoencoder = models.model_from_json(model_file.read())
#
#optimizer = SGD(lr=0.001, momentum=0.9, decay=0.0005, nesterov=False)
#autoencoder.compile(loss="categorical_crossentropy", optimizer=optimizer, metrics=['accuracy'])
#
#model.fit(train_images, train_labels, epochs=5)    
#
