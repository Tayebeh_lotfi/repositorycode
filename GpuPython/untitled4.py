import numpy as np
import h5py
matrix1 = np.random.random(size=(1000,1000))
matrix2 = np.random.random(size=(10000,100))

with h5py.File('../testgpupython/hdf5_data.h5','w') as hdf:
    hdf.create_dataset('dataset1',data=matrix1)
    hdf.create_dataset('dataset2',data=matrix2)
    
###########################################################################
    
with h5py.File('../testgpupython/hdf5_data.h5','r') as hdf:        
    ls = list(hdf.keys())
    print('List of datasets in this file: \n',ls)
    data=hdf.get('dataset1')
    dataset1 = np.array(data)
    print('shape of dataset1',dataset1.shape)

###########################################################################    
    
f = h5py.File('../testgpupython/hdf5_data.h5','r') 
ls = list(f.keys())
data = f.get('dataset1')
dataset1 = np.array(data)
print('shape of dataset1',dataset1.shape)
f.close()

###########################################################################

import panda as pd
hdf = pd.HDFStore('../testgpupython/hdf5_pandas.h5')