from __future__ import absolute_import, division, print_function
import os
# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

# Helper libraries
import numpy as np
import matplotlib.pyplot as plt 
import scipy.io as sio
import h5py


from tensorflow.keras import models
from tensorflow.keras.models import load_model
from tensorflow.keras import layers
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.layers import Activation, Reshape, Permute, Dense # keras.layers.core
from tensorflow.keras.layers import Conv2D, Convolution2D, MaxPooling2D, UpSampling2D # keras.layers.convolutional
from tensorflow.keras.layers import BatchNormalization

#img_w = 250
#img_h = 250
img_w = 256
img_h = 256
n_labels = 3
kernel = 3
num_epochs = 1
valnum = 50
class_names = ['ObjLayer1', 'ObjLayer2', 'ObjLayer3']

my_loss = np.zeros((10,50), dtype=np.float32)
my_val_loss = np.zeros((10,50), dtype=np.float32)
my_acc = np.zeros((10,50), dtype=np.float32)
my_val_acc = np.zeros((10,50), dtype=np.float32)
for jj in range(2,10):
    for ii in range(1,51): # 21):
    
        f = h5py.File('../Code/DataGeneration/SynthDATA/Ovlpv1_/cytoimage'+str(ii)+'.mat', 'r') 
        ls = list(f.keys())
        if ii==50:  
            data=f.get('test_images')
            data_ = np.array(data)
            test_images = np.float32(np.transpose(data_,(0,2,3,1)))
            
            data=f.get('test_labels')
            data_ = np.array(data)
            test_labels = np.float32(np.transpose(data_,(0,2,3,1)))
        
        data=f.get('train_images')
        #train_images = np.array(data)
        data_ = np.array(data)
        train_images = np.float32(np.transpose(data_,(0,2,3,1)))
        #train_images_ = train_images.astype(np.uint8)
        data=f.get('train_labels')
        #train_labels = np.array(data)    
        data_ = np.array(data)
        train_labels = np.float32(np.transpose(data_,(0,2,3,1)))
            
        f.close()
       
        
        model_ = [    
            Convolution2D(64, kernel_size=(kernel, kernel), padding='same', input_shape=( img_h, img_w,3),data_format="channels_last"), # border_mode='same',
            BatchNormalization(),
            Activation('relu'),
            Convolution2D(64, kernel_size=(kernel, kernel), padding='same',data_format="channels_last"),
            BatchNormalization(),
            Activation('relu'),
            MaxPooling2D(),
        
            Convolution2D(128, kernel_size=(kernel, kernel), padding='same', data_format="channels_last"),
            BatchNormalization(),
            Activation('relu'),
            Convolution2D(128, kernel_size=(kernel, kernel), padding='same', data_format="channels_last"),
            BatchNormalization(),
            Activation('relu'),
            MaxPooling2D(),
            ################################
            UpSampling2D(),
            Convolution2D(128, kernel_size=(kernel, kernel), padding='same', data_format="channels_last"),
            BatchNormalization(),
            Activation('relu'),
            Convolution2D(64, kernel_size=(kernel, kernel), padding='same', data_format="channels_last"),
            BatchNormalization(),
            Activation('relu'),
        
            UpSampling2D(),
            Convolution2D(64, kernel_size=(kernel, kernel), padding='same', data_format="channels_last"),
            BatchNormalization(),
            Activation('relu'),
            
            Convolution2D(n_labels, kernel_size=(1, 1), padding='same', data_format="channels_last"),
            BatchNormalization(),
        ]
        
        model = models.Sequential()
        for l in model_:
            model.add(l)
        
        model.add(Reshape((img_h, img_w, n_labels)))
        model.add(Activation('softmax'))
        
        optimizer = SGD(lr=0.001, momentum=0.9, decay=0.0005, nesterov=False)
        model.compile(loss='mean_squared_error', optimizer=optimizer, metrics=['accuracy'])
        print ('Compiled: OK')
        
        if (ii>1 or jj>0):
            model.load_weights('my_model_weights.h5')
        
            
        #model.compile(optimizer='adam', 
        #              loss='categorical_crossentropy',
        #              metrics=['accuracy'])
        train_imgs = train_images[valnum:]
        img_val = train_images[:valnum]
        train_lbls = train_labels[valnum:]
        lbl_val = train_labels[:valnum]
        
        history = model.fit(train_imgs, train_lbls, epochs=num_epochs, batch_size = 4, validation_data = (img_val,lbl_val)) 
        
        
    #    model.save('my_model.h5')  # creates a HDF5 file 'my_model.h5'
    #del model  # deletes the existing model
    
    ## returns a compiled model
    ## identical to the previous one
    #model = load_model('my_model.h5')
    
        model.save_weights('my_model_weights.h5')
    
    #model.load_weights('my_model_weights.h5')
        my_loss[jj,ii-1] = np.float32(history.history['loss'])
        my_val_loss[jj,ii-1] = np.float32(history.history['val_loss'])
        my_acc[jj,ii-1] = np.float32(history.history['acc'])
        my_val_acc[jj,ii-1] = np.float32(history.history['val_acc'])


#loss = history.history['loss']
#val_loss = history.history['val_loss']
#epochs = range(1,len(loss)+1)
#plt.plot(epochs,loss,'bo',label = 'Training Loss')
#plt.plot(epochs,val_loss,'b',label = 'Validation Loss')
#plt.title('Training and validation loss')
#plt.xlabel('Epochs')
#plt.ylabel('Loss')
#plt.legend()
#plt.show()
#
#plt.clf()
#acc = history.history['acc']
#val_acc = history.history['val_acc']
#plt.plot(epochs,acc,'bo',label = 'Training acc')
#plt.plot(epochs,val_acc,'b',label = 'Validation acc')
#plt.title('Training and validation accuracy')
#plt.xlabel('Epochs')
#plt.ylabel('Acc')
#plt.legend()
#plt.show()


model.save('my_model.h5')

predictions = model.predict(test_images)
results = model.evaluate(test_images,test_labels)

plt.figure()
plt.imshow(predictions[0,:,:,0])
plt.colorbar()
plt.grid(False)
plt.show()


#sio.savemat('prediction_train', predictions)
sio.savemat('PredictionTrainSequential.mat', {'predictions':predictions,'test_images':test_images,'test_labels':test_labels})
#sio.savemat('prediction_trainmore.mat', {'predictions':predictions,'test_images':test_images,'test_labels':test_labels})
#sio.savemat('prediction_train', predictions, appendmat=True, format='5', long_field_names=False, do_compression=False, oned_as='row')


