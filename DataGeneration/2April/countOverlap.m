function [result,AllPix,Overlap] = countOverlap(path,number)
    S = imread([path,num2str(number),'.png']);
    Overlap = 0;
    AllPix = numel(find(S>0));
    CC1 = bwconncomp(S(:,:,1),8);
    if(numel(S(:,:,2)~=0))
        CC2 = bwconncomp(S(:,:,2),8);
        for ii = 1:CC1.NumObjects
            Id1 = CC1.PixelIdxList{ii};
            for jj = 1:CC2.NumObjects
                Id2 = CC2.PixelIdxList{jj};
                Overlap = Overlap + numel(intersect(Id1,Id2)); 
            end
        end
        if(numel(S(:,:,3)~=0))
            CC3 = bwconncomp(S(:,:,3),8);
            for ii = 1:CC1.NumObjects
                Id1 = CC1.PixelIdxList{ii};
                for jj = 1:CC3.NumObjects
                    Id3 = CC3.PixelIdxList{jj};
                    Overlap = Overlap + numel(intersect(Id1,Id3));
                end
            end
            for ii = 1:CC2.NumObjects
                Id2 = CC2.PixelIdxList{ii};
                for jj = 1:CC3.NumObjects
                    Id3 = CC3.PixelIdxList{jj};
                    Overlap = Overlap + numel(intersect(Id2,Id3));
                end
            end
        end
    end
    result = Overlap / AllPix;
end