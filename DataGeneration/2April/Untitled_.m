mask1 = (S(:,:,1)+S(:,:,2)+S(:,:,3))>0;
mask2 = (I(:,:,1)+I(:,:,2)+I(:,:,3))>0;

figure, imshow(abs(mask1-mask2))
figure, subplot 121, imshow(uint8(I)), subplot 122, imshow(S)


figure
for i = 1:4
    imshow(J(:,:,:,i))
    pause
end



figure, subplot 121, imshow(I1_), subplot 122, imshow(imrotate(I1_,90,'bicubic'))

tar = I1;
tar_seg = S1;

mask1 = (tar(:,:,1)>0 | tar(:,:,2)>0 | tar(:,:,3) > 0);
mask2 = (tar_seg>0);
figure, imshow(abs(mask1-mask2))

tar = I;
tar_seg = S;
mask1 = (tar(:,:,1)>0 | tar(:,:,2)>0 | tar(:,:,3) > 0);
mask2 = (tar_seg(:,:,1)>0 | tar_seg(:,:,2)>0 | tar_seg(:,:,3) > 0);
figure, imshow(abs(mask1-mask2))



%%
% tar = I1;
tar_seg = S1;
nsize1 = 300*300;
idx = 1:nsize1;
L1 = (zeros(300,300,3));
L1(idx) = 230 + fix(2*randn(nsize1,1)); % 190
L1(idx+nsize1) = 226 + fix(2*randn(nsize1,1)); % 186
L1(idx+2*nsize1) = 223 + fix(2*randn(nsize1,1)); % 183
mask11 = 1-(tar_seg>0); % | S1(:,:,2)>0 | S1(:,:,3) >0 );
tar = double(I1);
figure, subplot 121, imshow(uint8(mask11.*L1+tar)); subplot 122, imshow(G)

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

index12 = find(S11>0 & S22>0 & S33==0);
index13 = find(S11>0 & S22==0 & S33>0);
index23 = find(S11==0 & S22>0 & S33>0);
index123 = find(S11>0 & S22>0 & S33>0);

I = I11 + I22 + I33;
I(index12) = I(index12) - alpha*I11(index12) - beta*I22(index12);
I(index12+nsize) = I(index12+nsize) - alpha*I11(index12+nsize) - beta*I22(index12+nsize);
I(index12+2*nsize) = I(index12+2*nsize) - alpha*I11(index12+2*nsize) - beta*I22(index12+2*nsize);

I(index13) = I(index13) - alpha*I11(index13) - gamma*I33(index13);
I(index13+nsize) = I(index13+nsize) - alpha*I11(index13+nsize) - gamma*I33(index13+nsize);
I(index13+2*nsize) = I(index13+2*nsize) - alpha*I11(index13+2*nsize) - gamma*I33(index13+2*nsize);

I(index23) = I(index23) - beta*I22(index23) - gamma*I33(index23);
I(index23+nsize) = I(index23+nsize) - beta*I22(index23+nsize) - gamma*I33(index23+nsize);
I(index23+2*nsize) = I(index23+2*nsize) - beta*I22(index23+2*nsize) - gamma*I33(index23+2*nsize);

I(index123) = I(index123) + alpha*I11(index123) + beta*I22(index123) + gamma*I33(index123);
I(index123+nsize) = I(index123+nsize) + alpha*I11(index123+nsize) + beta*I22(index123+nsize) + gamma*I33(index123+nsize);
I(index123+2*nsize) = I(index123+2*nsize) + alpha*I11(index123+2*nsize) + beta*I22(index123+2*nsize) + gamma*I33(index123+2*nsize);


S(:,:,1) = S11;
S(:,:,2) = S22;
S(:,:,3) = S33;     

%%
index12 = find(S11>0 & S22>0);

I = I11 + I22;
I(index12) = I(index12) - alpha*I11(index12) - beta*I22(index12);
I(index12+nsize) = I(index12+nsize) - alpha*I11(index12+nsize) - beta*I22(index12+nsize);
I(index12+2*nsize) = I(index12+2*nsize) - alpha*I11(index12+2*nsize) - beta*I22(index12+2*nsize);

S(:,:,1) = S11;
S(:,:,2) = S22;
S(:,:,3) = 0;     


%%

% B = bwboundaries(S11,8,'noholes');
% tst = I11;
se = strel('disk',3);
S11_ = imerode(S11,se);
indx = find(abs(S11_ - S11)>0);
B = imgaussfilt(I11,5,'FilterSize',5);
I11(indx) = B(indx); I11(indx+nsize) = B(indx+nsize); I11(indx+2*nsize) = B(indx+2*nsize);

se = strel('disk',3);
S22_ = imerode(S22,se);
indx = find(abs(S22_ - S22)>0);
B = imgaussfilt(I22,5,'FilterSize',5);
I22(indx) = B(indx); I22(indx+nsize) = B(indx+nsize); I22(indx+2*nsize) = B(indx+2*nsize);

se = strel('disk',3);
S33_ = imerode(S33,se);
indx = find(abs(S33_ - S33)>0);
B = imgaussfilt(I33,5,'FilterSize',5);
I33(indx) = B(indx); I33(indx+nsize) = B(indx+nsize); I33(indx+2*nsize) = B(indx+2*nsize);


tar_seg = S11;
nsize1 = 350*350;
idx = 1:nsize1;
L1 = (zeros(350,350,3));
L1(idx) = 235 + fix(2*randn(nsize1,1)); % 190
L1(idx+nsize1) = 240 + fix(2*randn(nsize1,1)); % 186
L1(idx+2*nsize1) = 235 + fix(2*randn(nsize1,1)); % 183
mask11 = 1-(tar_seg>0); % | S1(:,:,2)>0 | S1(:,:,3) >0 );
tar = double(tst);
figure, subplot 121, imshow(uint8(mask11.*L1+tar)); subplot 122, imshow(uint8(mask11.*L1+I11))

%%
CC1 = bwconncomp(S(:,:,1),8);
CC2 = bwconncomp(S(:,:,2),8);
CC3 = bwconncomp(S(:,:,3),8);
Sn = zeros(size(S));
Sn(:,:,1) = S(:,:,1);
nsize = 350*350;
for ii = 1:CC2.NumObjects
    Id2 = CC2.PixelIdxList{ii};
    cnt = 0;
    for jj = 1:CC1.NumObjects
        Id1 = CC1.PixelIdxList{jj};
        if(numel(intersect(Id1,Id2))~=0)
            cnt = cnt + 1;
        end
    end
    if(cnt == 0)
        Sn(Id2) = S(Id2 + nsize);
    else
        Sn(Id2+nsize) = S(Id2+nsize);
    end
end

CC1 = bwconncomp(Sn(:,:,1),8);
CC2 = bwconncomp(Sn(:,:,2),8);
% CC3 = bwconncomp(S(:,:,3),8);



for ii = 1:CC3.NumObjects
    Id3 = CC3.PixelIdxList{ii};
    cnt13 = 0;
    for jj = 1:CC1.NumObjects
        Id1 = CC1.PixelIdxList{jj};
        if(numel(intersect(Id1,Id3))~=0)
            cnt13 = cnt13 + 1;
        end
    end
    if(cnt13 == 0)
        Sn(Id3) = S(Id3 + 2*nsize);
    else
        cnt23 = 0;
        for kk = 1:CC2.NumObjects
            Id2 = CC2.PixelIdxList{kk};
            if(numel(intersect(Id2,Id3))~=0)
                cnt23 = cnt23 + 1;
            end
        end
        if(cnt23 ==0)
            Sn(Id3+nsize) = S(Id3 + 2*nsize);
        else
            Sn(Id3 + 2*nsize) = S(Id3 + 2*nsize);
        end
    end     
end


%%
nsize = 350*350;
idx = 1:nsize;
L = (zeros(350,350,3));
L(idx) = 235 + fix(2*randn(nsize,1)); % 190
L(idx+nsize) = 240 + fix(2*randn(nsize,1)); % 186
L(idx+2*nsize) = 235 + fix(2*randn(nsize,1)); % 183
mask1 = 1-(S11>0 );
Img1 =  uint8(mask1.*L+I11);


tar_seg = S1;
nsize1 = 300*300;
idx1 = 1:nsize1;
L1 = (zeros(300,300,3));
L1(idx1) = 235 + fix(2*randn(nsize1,1)); % 190
L1(idx1+nsize1) = 240 + fix(2*randn(nsize1,1)); % 186
L1(idx1+2*nsize1) = 235 + fix(2*randn(nsize1,1)); % 183
mask11 = 1-(tar_seg>0); % | S1(:,:,2)>0 | S1(:,:,3) >0 );
tar = double(I1);
Img11 = uint8(mask11.*L1+tar);
figure, 
subplot 121, imshow(Img11)
subplot 122, imshow(Img1(1:300,1:300,:));




pth1 = '../SynthDATA/Images/img';
pth2 = '../SynthDATA/Segments/seg';

for ii = 1:2000
    I(:,:,:,ii) = imread([pth1,num2str(ii),'.png']);
    S(:,:,ii) = imread([pth2,num2str(ii),'.png']);
end

save('imagesOrg.mat','I','S');

cnt = 0;
for iter = 1:1000
    J = niftiread(['../SynthDATA\Ovlp_\SegTst\seg',num2str(iter),'.nii']);
    if(numel(find(J(:,:,3)>0))~=0)
%         iter
        cnt = cnt+1;
    end
end

%%
%     sigma = 15;
%     fltrsz = 7;
%     dsksz = 3;
%         se = strel('disk',dsksz);
%         S11_ = imerode(S11,se);
%         indx = find(abs(S11_ - S11)>0);
%         B = imgaussfilt(I11,sigma,'FilterSize',fltrsz);
%         I11(indx) = B(indx); I11(indx+nsize) = B(indx+nsize); I11(indx+2*nsize) = B(indx+2*nsize);
% 
%         se = strel('disk',dsksz);
%         S22_ = imerode(S22,se);
%         indx = find(abs(S22_ - S22)>0);
%         B = imgaussfilt(I22,sigma,'FilterSize',fltrsz);
%         I22(indx) = B(indx); I22(indx+nsize) = B(indx+nsize); I22(indx+2*nsize) = B(indx+2*nsize);