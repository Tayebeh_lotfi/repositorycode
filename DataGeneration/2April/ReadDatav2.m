function [I,S] = ReadDatav2(path1,path2,k,len1,wid1,stp,nsize,number,alpha,beta,gamma,theta) % ,rw,col)
    if k==3
        [I1,S1] = ImageRd(path1,path2,number(1));
        [I2,S2] = ImageRd(path1,path2,number(2));
        [I3,S3] = ImageRd(path1,path2,number(3));
        I11 = zeros(len1,wid1,3);
        S11 = zeros(len1,wid1);
        I22 = zeros(len1,wid1,3);
        S22 = zeros(len1,wid1);
        I33 = zeros(len1,wid1,3);
        S33 = zeros(len1,wid1);
        
        I11(1:size(I1,1),1:size(I1,2),:) = I1;
        S11(1:size(S1,1),1:size(S1,2)) = S1;
        I22(stp+1:size(I2,1)+stp,stp+1:size(I2,2)+stp,:) = I2;
        S22(stp+1:size(S2,1)+stp,stp+1:size(S2,2)+stp) = S2;        
        I33(2*stp+1:size(I3,1)+2*stp,2*stp+1:size(I3,2)+2*stp,:) = I3;
        S33(2*stp+1:size(S3,1)+2*stp,2*stp+1:size(S3,2)+2*stp) = S3;        
                
        S(:,:,1) = 1*(S11>0);
        S(:,:,2) = 2*(S22>0);
        S(:,:,3) = 3*(S33>0);
        [Img1,Img2,Img3,Seg] = finalizeSeg(I11,I22,I33,S,nsize);
        I = Img1 + Img2 + Img3;
        
        index12 = find(Seg(:,:,1)>0 & Seg(:,:,2)>0 & Seg(:,:,3)==0);
        index13 = find(Seg(:,:,1)>0 & Seg(:,:,2)==0 & Seg(:,:,3)>0);
        index23 = find(Seg(:,:,1)==0 & Seg(:,:,2)>0 & Seg(:,:,3)>0);
        index123 = find(Seg(:,:,1)>0 & Seg(:,:,2)>0 & Seg(:,:,3)>0);
        
        I(index12) = I(index12) - alpha*Img1(index12) - beta*Img2(index12);
        I(index12+nsize) = I(index12+nsize) - alpha*Img1(index12+nsize) - beta*Img2(index12+nsize);
        I(index12+2*nsize) = I(index12+2*nsize) - alpha*Img1(index12+2*nsize) - beta*Img2(index12+2*nsize);

        I(index13) = I(index13) - alpha*Img1(index13) - gamma*Img3(index13);
        I(index13+nsize) = I(index13+nsize) - alpha*Img1(index13+nsize) - gamma*Img3(index13+nsize);
        I(index13+2*nsize) = I(index13+2*nsize) - alpha*Img1(index13+2*nsize) - gamma*Img3(index13+2*nsize);

        I(index23) = I(index23) - beta*Img2(index23) - gamma*Img3(index23);
        I(index23+nsize) = I(index23+nsize) - beta*Img2(index23+nsize) - gamma*Img3(index23+nsize);
        I(index23+2*nsize) = I(index23+2*nsize) - beta*Img2(index23+2*nsize) - gamma*Img3(index23+2*nsize);

        I(index123) = I(index123) - alpha*Img1(index123) - beta*Img2(index123) - gamma*Img3(index123);
        I(index123+nsize) = I(index123+nsize) - alpha*Img1(index123+nsize) - beta*Img2(index123+nsize) - gamma*Img3(index123+nsize);
        I(index123+2*nsize) = I(index123+2*nsize) - alpha*Img1(index123+2*nsize) - beta*Img2(index123+2*nsize) - gamma*Img3(index123+2*nsize);

        S = Seg;             
    end   
     if k==4
        [I1,S1] = ImageRd(path1,path2,number(1));
        [I2,S2] = ImageRd(path1,path2,number(2));
        [I3,S3] = ImageRd(path1,path2,number(3));
        [I4,S4] = ImageRd(path1,path2,number(3));
        I11 = zeros(len1,wid1,3);
        S11 = zeros(len1,wid1);
        I22 = zeros(len1,wid1,3);
        S22 = zeros(len1,wid1);
        I33 = zeros(len1,wid1,3);
        S33 = zeros(len1,wid1);
        I44 = zeros(len1,wid1,3);
        S44 = zeros(len1,wid1);
        
        I11(1:size(I1,1),1:size(I1,2),:) = I1;
        S11(1:size(S1,1),1:size(S1,2)) = S1;
        I22(stp+1:size(I2,1)+stp,stp+1:size(I2,2)+stp,:) = I2;
        S22(stp+1:size(S2,1)+stp,stp+1:size(S2,2)+stp) = S2;        
        I33(2*stp+1:size(I3,1)+2*stp,2*stp+1:size(I3,2)+2*stp,:) = I3;
        S33(2*stp+1:size(S3,1)+2*stp,2*stp+1:size(S3,2)+2*stp) = S3;        
        I44(3*stp+1:size(I4,1)+3*stp,3*stp+1:size(I4,2)+3*stp,:) = I4;
        S44(3*stp+1:size(S4,1)+3*stp,3*stp+1:size(S4,2)+3*stp) = S4;     
        
        S(:,:,1) = 1*(S11>0);
        S(:,:,2) = 2*(S22>0);
        S(:,:,3) = 3*(S33>0);
        S(:,:,4) = 4*(S44>0);
        [Img1,Img2,Img3,Img4,Seg] = finalizeSegv1(I11,I22,I33,I44,S,nsize);
        I = Img1 + Img2 + Img3 + Img4;
        
        index12 = find(Seg(:,:,1)>0 & Seg(:,:,2)>0 & Seg(:,:,3)==0 & Seg(:,:,4)==0);
        index13 = find(Seg(:,:,1)>0 & Seg(:,:,2)==0 & Seg(:,:,3)>0 & Seg(:,:,4)==0);
        index14 = find(Seg(:,:,1)>0 & Seg(:,:,2)==0 & Seg(:,:,3)==0 & Seg(:,:,4)>0);
        index23 = find(Seg(:,:,1)==0 & Seg(:,:,2)>0 & Seg(:,:,3)>0 & Seg(:,:,4)==0);
        index24 = find(Seg(:,:,1)==0 & Seg(:,:,2)>0 & Seg(:,:,3)==0 & Seg(:,:,4)>0);
        index34 = find(Seg(:,:,1)==0 & Seg(:,:,2)==0 & Seg(:,:,3)>0 & Seg(:,:,4)>0);
        index123 = find(Seg(:,:,1)>0 & Seg(:,:,2)>0 & Seg(:,:,3)>0 & Seg(:,:,4)==0);
        index124 = find(Seg(:,:,1)>0 & Seg(:,:,2)>0 & Seg(:,:,3)==0 & Seg(:,:,4)>0);
        index134 = find(Seg(:,:,1)>0 & Seg(:,:,2)==0 & Seg(:,:,3)>0 & Seg(:,:,4)>0);
        index234 = find(Seg(:,:,1)==0 & Seg(:,:,2)>0 & Seg(:,:,3)>0 & Seg(:,:,4)>0);
        index1234 = find(Seg(:,:,1)>0 & Seg(:,:,2)>0 & Seg(:,:,3)>0 & Seg(:,:,4)>0);
        
        I(index12) = I(index12) - alpha*Img1(index12) - beta*Img2(index12);
        I(index12+nsize) = I(index12+nsize) - alpha*Img1(index12+nsize) - beta*Img2(index12+nsize);
        I(index12+2*nsize) = I(index12+2*nsize) - alpha*Img1(index12+2*nsize) - beta*Img2(index12+2*nsize);

        I(index13) = I(index13) - alpha*Img1(index13) - gamma*Img3(index13);
        I(index13+nsize) = I(index13+nsize) - alpha*Img1(index13+nsize) - gamma*Img3(index13+nsize);
        I(index13+2*nsize) = I(index13+2*nsize) - alpha*Img1(index13+2*nsize) - gamma*Img3(index13+2*nsize);

        I(index14) = I(index14) - alpha*Img1(index14) - theta*Img4(index14);
        I(index14+nsize) = I(index14+nsize) - alpha*Img1(index14+nsize) - theta*Img4(index14+nsize);
        I(index14+2*nsize) = I(index14+2*nsize) - alpha*Img1(index14+2*nsize) - theta*Img4(index14+2*nsize);
        
        I(index23) = I(index23) - beta*Img2(index23) - gamma*Img3(index23);
        I(index23+nsize) = I(index23+nsize) - beta*Img2(index23+nsize) - gamma*Img3(index23+nsize);
        I(index23+2*nsize) = I(index23+2*nsize) - beta*Img2(index23+2*nsize) - gamma*Img3(index23+2*nsize);

        I(index24) = I(index24) - beta*Img2(index24) - theta*Img4(index24);
        I(index24+nsize) = I(index24+nsize) - beta*Img2(index24+nsize) - theta*Img4(index24+nsize);
        I(index24+2*nsize) = I(index24+2*nsize) - beta*Img2(index24+2*nsize) - theta*Img4(index24+2*nsize);
        
        I(index34) = I(index34) - gamma*Img3(index34) - theta*Img4(index34);
        I(index34+nsize) = I(index34+nsize) - gamma*Img3(index34+nsize) - theta*Img4(index34+nsize);
        I(index34+2*nsize) = I(index34+2*nsize) - gamma*Img3(index34+2*nsize) - theta*Img4(index34+2*nsize);
        
        I(index123) = I(index123) - alpha*Img1(index123) - beta*Img2(index123) - gamma*Img3(index123);
        I(index123+nsize) = I(index123+nsize) - alpha*Img1(index123+nsize) - beta*Img2(index123+nsize) - gamma*Img3(index123+nsize);
        I(index123+2*nsize) = I(index123+2*nsize) - alpha*Img1(index123+2*nsize) - beta*Img2(index123+2*nsize) - gamma*Img3(index123+2*nsize);
        
        I(index124) = I(index124) - alpha*Img1(index124) - beta*Img2(index124) -theta*Img4(index124);
        I(index124+nsize) = I(index124+nsize) - alpha*Img1(index124+nsize) - beta*Img2(index124+nsize) - theta*Img4(index124+nsize);
        I(index124+2*nsize) = I(index124+2*nsize) - alpha*Img1(index124+2*nsize) - beta*Img2(index124+2*nsize) - theta*Img4(index124+2*nsize);
        
        I(index134) = I(index134) - alpha*Img1(index134) - gamma*Img3(index134) -theta*Img4(index134);
        I(index134+nsize) = I(index134+nsize) - alpha*Img1(index134+nsize) - gamma*Img3(index134+nsize) - theta*Img4(index134+nsize);
        I(index134+2*nsize) = I(index134+2*nsize) - alpha*Img1(index134+2*nsize) - gamma*Img3(index134+2*nsize) - theta*Img4(index134+2*nsize);
        
        I(index234) = I(index234) - beta*Img2(index234) - gamma*Img3(index234) - theta*Img4(index234);
        I(index234+nsize) = I(index234+nsize) - beta*Img2(index234+nsize) - gamma*Img3(index234+nsize) - theta*Img4(index234+nsize);
        I(index234+2*nsize) = I(index234+2*nsize) - beta*Img2(index234+2*nsize) - gamma*Img3(index234+2*nsize) - theta*Img4(index234+2*nsize);        

        I(index1234) = I(index1234) - alpha*Img1(index1234) - beta*Img2(index1234) - gamma*Img3(index1234) -theta*Img4(index1234);
        I(index1234+nsize) = I(index1234+nsize) - alpha*Img1(index1234+nsize) - beta*Img2(index1234+nsize) - gamma*Img3(index1234+nsize) - theta*Img4(index1234+nsize);
        I(index1234+2*nsize) = I(index1234+2*nsize) - alpha*Img1(index1234+2*nsize) - beta*Img2(index1234+nsize) - gamma*Img3(index1234+2*nsize) - theta*Img4(index1234+2*nsize);          
        
        S = Seg;             
     end
end