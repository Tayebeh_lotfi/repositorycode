function [I,S] = ReadData(path1,path2,k,len1,wid1,stp,nsize,number,alpha,beta,gamma) % ,rw,col)

    if(k==2)
%         [I1,S1] = ImageMdf(path1,path2,number(1));
%         [I2,S2] = ImageMdf(path1,path2,number(2));
        [I1,S1] = ImageRd(path1,path2,number(1));
        [I2,S2] = ImageRd(path1,path2,number(2));
        
        I11 = zeros(len1,wid1,3);
        S11 = zeros(len1,wid1);
        I22 = zeros(len1,wid1,3);
        S22 = zeros(len1,wid1);
        
        I11(1:size(I1,1),1:size(I1,2),:) = I1;
        S11(1:size(S1,1),1:size(S1,2)) = S1;
        I22(stp+1:size(I2,1)+stp,stp+1:size(I2,2)+stp,:) = I2;
        S22(stp+1:size(S2,1)+stp,stp+1:size(S2,2)+stp) = S2;

        S(:,:,1) = 1*(S11>0);
        S(:,:,2) = 2*(S22>0);
        S(:,:,3) = 0;
        [Img1,Img2,~,Seg] = finalizeSeg(I11,I22,zeros(size(I11)),S,nsize);                
        index12 = find(Seg(:,:,1)>0 & Seg(:,:,2)>0);
                
        I = Img1 + Img2;
        I(index12) = I(index12) - alpha*Img1(index12) - beta*Img2(index12);
        I(index12+nsize) = I(index12+nsize) - alpha*Img1(index12+nsize) - beta*Img2(index12+nsize);
        I(index12+2*nsize) = I(index12+2*nsize) - alpha*Img1(index12+2*nsize) - beta*Img2(index12+2*nsize);
        S = Seg;
        
    end    
    if k==3
        [I1,S1] = ImageRd(path1,path2,number(1));
        [I2,S2] = ImageRd(path1,path2,number(2));
        [I3,S3] = ImageRd(path1,path2,number(3));
        I11 = zeros(len1,wid1,3);
        S11 = zeros(len1,wid1);
        I22 = zeros(len1,wid1,3);
        S22 = zeros(len1,wid1);
        I33 = zeros(len1,wid1,3);
        S33 = zeros(len1,wid1);
        
        I11(1:size(I1,1),1:size(I1,2),:) = I1;
        S11(1:size(S1,1),1:size(S1,2)) = S1;
        I22(stp+1:size(I2,1)+stp,stp+1:size(I2,2)+stp,:) = I2;
        S22(stp+1:size(S2,1)+stp,stp+1:size(S2,2)+stp) = S2;        
        I33(2*stp+1:size(I3,1)+2*stp,2*stp+1:size(I3,2)+2*stp,:) = I3;
        S33(2*stp+1:size(S3,1)+2*stp,2*stp+1:size(S3,2)+2*stp) = S3;        
                
        S(:,:,1) = 1*(S11>0);
        S(:,:,2) = 2*(S22>0);
        S(:,:,3) = 3*(S33>0);
        [Img1,Img2,Img3,Seg] = finalizeSeg(I11,I22,I33,S,nsize);
        I = Img1 + Img2 + Img3;
        
        index12 = find(Seg(:,:,1)>0 & Seg(:,:,2)>0 & Seg(:,:,3)==0);
        index13 = find(Seg(:,:,1)>0 & Seg(:,:,2)==0 & Seg(:,:,3)>0);
        index23 = find(Seg(:,:,1)==0 & Seg(:,:,2)>0 & Seg(:,:,3)>0);
        index123 = find(Seg(:,:,1)>0 & Seg(:,:,2)>0 & Seg(:,:,3)>0);
        
        I(index12) = I(index12) - alpha*Img1(index12) - beta*Img2(index12);
        I(index12+nsize) = I(index12+nsize) - alpha*Img1(index12+nsize) - beta*Img2(index12+nsize);
        I(index12+2*nsize) = I(index12+2*nsize) - alpha*Img1(index12+2*nsize) - beta*Img2(index12+2*nsize);

        I(index13) = I(index13) - alpha*Img1(index13) - gamma*Img3(index13);
        I(index13+nsize) = I(index13+nsize) - alpha*Img1(index13+nsize) - gamma*Img3(index13+nsize);
        I(index13+2*nsize) = I(index13+2*nsize) - alpha*Img1(index13+2*nsize) - gamma*Img3(index13+2*nsize);

        I(index23) = I(index23) - beta*Img2(index23) - gamma*Img3(index23);
        I(index23+nsize) = I(index23+nsize) - beta*Img2(index23+nsize) - gamma*Img3(index23+nsize);
        I(index23+2*nsize) = I(index23+2*nsize) - beta*Img2(index23+2*nsize) - gamma*Img3(index23+2*nsize);

        I(index123) = I(index123) - alpha*Img1(index123) - beta*Img2(index123) - gamma*Img3(index123);
        I(index123+nsize) = I(index123+nsize) - alpha*Img1(index123+nsize) - beta*Img2(index123+nsize) - gamma*Img3(index123+nsize);
        I(index123+2*nsize) = I(index123+2*nsize) - alpha*Img1(index123+2*nsize) - beta*Img2(index123+2*nsize) - gamma*Img3(index123+2*nsize);

        S = Seg;             
    end   
%%    
%     I_ = imresize(I,[rw,col],'bicubic');
%     S_ = imresize(S,[rw,col],'bicubic');
%     
%     S1_ = zeros(size(S_));
%     for i = 1:3
%         temp = S_(:,:,i);
%         temp(temp~=0)=i;
%         S1_(:,:,i) = temp;
%     end
%     S_ = S1_;
%     indx = find(I_<=0); I_(indx) = -I(indx); 
% %     indx = find(I_(:,:,2)<=0); I_(indx+rw*col) = -I_(indx+rw*col); 
% %     indx = find(I_(:,:,3)<=0); I_(indx+2*rw*col) = -I_(indx+2*rw*col);
% % %     S_(indx) = 0; S_(indx+rw*col) = 0; S_(indx+2*rw*col) = 0;
% %     
% % %     mask1 = (I_(:,:,1)>0 | I_(:,:,2)>0 | I_(:,:,3) > 0);
% % %     mask2 = (S_(:,:,1)>0 | S_(:,:,2)>0 | S_(:,:,3) > 0);
% % %     indx = find(abs(mask1-mask2)>0);
% % %     I_(indx) = 0; I_(indx+rw*col) = 0; I_(indx+2*rw*col) = 0;
% % %     S_(indx) = 0; S_(indx+rw*col) = 0; S_(indx+2*rw*col) = 0;   
% % %     
% % % %     I_ = I; S_ = S;
end