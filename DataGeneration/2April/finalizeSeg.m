function [Img1,Img2,Img3,Seg] = finalizeSeg(Img1,Img2,Img3,Seg,nsize)
    CC1 = bwconncomp(Seg(:,:,1),8);
    p = .6;
    if(numel(Seg(:,:,2))~=0)
        CC2 = bwconncomp(Seg(:,:,2),8);

%         nsize = 350*350;
        ii = 1;
    %     jj = 1;
        while ii <= CC2.NumObjects
            Id2 = CC2.PixelIdxList{ii};
            jj=1;
    %         flg = 0;
            while jj <= CC1.NumObjects
                Id1 = CC1.PixelIdxList{jj};
                if(numel(intersect(Id1,Id2)) > p*numel(Id1))
                    Seg(Id1) = 0;
                    Img1(Id1) = 0; Img1(Id1+nsize) = 0; Img1(Id1+2*nsize) = 0;
                    jj = jj+1;
                else 
                    if(numel(intersect(Id1,Id2)) > p*numel(Id2))
                        Seg(Id2+nsize) = 0;
                        Img2(Id2) = 0; Img2(Id2+nsize) = 0; Img2(Id2+2*nsize) = 0;
                        ii = ii+1;
                        if(ii <= CC2.NumObjects)
                            Id2 = CC2.PixelIdxList{ii};
                            jj=1;
                        else
                            break;
                        end
                    else
                        jj = jj+1;

                    end
                end
            end
            ii = ii+1;
        end

    
    
        if(numel(Seg(:,:,3))~=0)
            CC1 = bwconncomp(Seg(:,:,1),8);
            CC3 = bwconncomp(Seg(:,:,3),8);
            ii = 1;

            while ii <= CC3.NumObjects
                Id3 = CC3.PixelIdxList{ii};
                jj = 1;
                while jj <= CC1.NumObjects
                    Id1 = CC1.PixelIdxList{jj};
                    if(numel(intersect(Id1,Id3)) > p*numel(Id1))
                        Seg(Id1) = 0; 
                        Img1(Id1) = 0; Img1(Id1+nsize) = 0; Img1(Id1+2*nsize) = 0;
                        jj = jj+1;
                    else 
                        if(numel(intersect(Id1,Id3)) > p*numel(Id3))
                            Seg(Id3+2*nsize) = 0;
                            Img3(Id3) = 0; Img3(Id3+nsize) = 0; Img3(Id3+2*nsize) = 0;
                            ii = ii+1;
                            if(ii <= CC3.NumObjects)
                                Id3 = CC3.PixelIdxList{ii};
                                jj=1;
                            else
                                break;
                            end
                        else
                            jj = jj+1;

                        end
                    end
                end
                ii = ii+1;
            end
            CC2 = bwconncomp(Seg(:,:,2),8);
            CC3 = bwconncomp(Seg(:,:,3),8);
            ii = 1;

            while ii <= CC3.NumObjects
                Id3 = CC3.PixelIdxList{ii};
                jj=1;
                while jj <= CC2.NumObjects
                    Id2 = CC2.PixelIdxList{jj};
                    if(numel(intersect(Id2,Id3)) > p*numel(Id2))
                        Seg(Id2+nsize) = 0; 
                        Img2(Id2) = 0; Img2(Id2+nsize) = 0; Img2(Id2+2*nsize) = 0;
                        jj = jj+1;
                    else 
                        if(numel(intersect(Id2,Id3)) > p*numel(Id3))
                            Seg(Id3+2*nsize) = 0;
                            Img3(Id3) = 0; Img3(Id3+nsize) = 0; Img3(Id3+2*nsize) = 0;
                            ii = ii+1;
                            if(ii <= CC3.NumObjects)
                                Id3 = CC3.PixelIdxList{ii};
                                jj=1;
                            else
                                break;
                            end
                        else
                            jj = jj+1;

                        end
                    end
                end
                ii = ii+1;
            end

        end      
    end
end