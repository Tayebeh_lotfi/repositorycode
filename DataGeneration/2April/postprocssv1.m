function S = postprocssv1(Seg,k,nsize)
    CC1 = bwconncomp(Seg(:,:,1),8);
    CC2 = bwconncomp(Seg(:,:,2),8);
    CC3 = bwconncomp(Seg(:,:,3),8);
    CC4 = bwconncomp(Seg(:,:,4),8);
    S = zeros(size(Seg));
    S(:,:,1) = Seg(:,:,1);
%     nsize = 350*350;
    for ii = 1:CC2.NumObjects
        Id2 = CC2.PixelIdxList{ii};
        cnt = 0;
        for jj = 1:CC1.NumObjects
            Id1 = CC1.PixelIdxList{jj};
            if(numel(intersect(Id1,Id2))~=0)
                cnt = cnt + 1;
            end
        end
        if(cnt == 0)
            S(Id2) = 1; % Seg(Id2 + nsize);
        else
            S(Id2+nsize) = 2; % Seg(Id2+nsize);
        end
    end
    if(k>=3)
        CC1 = bwconncomp(S(:,:,1),8);
        CC2 = bwconncomp(S(:,:,2),8);

        for ii = 1:CC3.NumObjects
            Id3 = CC3.PixelIdxList{ii};
            cnt13 = 0;
            for jj = 1:CC1.NumObjects
                Id1 = CC1.PixelIdxList{jj};
                if(numel(intersect(Id1,Id3))~=0)
                    cnt13 = cnt13 + 1;
                end
            end
            if(cnt13 == 0)
                S(Id3) = 1; % Seg(Id3 + 2*nsize);
            else
                cnt23 = 0;
                for kk = 1:CC2.NumObjects
                    Id2 = CC2.PixelIdxList{kk};
                    if(numel(intersect(Id2,Id3))~=0)
                        cnt23 = cnt23 + 1;
                    end
                end
                if(cnt23 ==0)
                    S(Id3+nsize) = 2; % Seg(Id3 + 2*nsize);
                else
                    S(Id3 + 2*nsize) = 3; % Seg(Id3 + 2*nsize);
                end
            end     
        end
    end
    if(k>=4)
        CC1 = bwconncomp(S(:,:,1),8);
        CC2 = bwconncomp(S(:,:,2),8);
        CC3 = bwconncomp(S(:,:,3),8);
        for ii = 1:CC4.NumObjects
            Id4 = CC4.PixelIdxList{ii};
            cnt14 = 0;
            for jj = 1:CC1.NumObjects
                Id1 = CC1.PixelIdxList{jj};
                if(numel(intersect(Id1,Id4))~=0)
                    cnt14 = cnt14 + 1;
                end
            end
            if(cnt14 == 0)
                S(Id4) = 1; % Seg(Id3 + 2*nsize);
            else
                cnt24 = 0;
                for kk = 1:CC2.NumObjects
                    Id2 = CC2.PixelIdxList{kk};
                    if(numel(intersect(Id2,Id4))~=0)
                        cnt24 = cnt24 + 1;
                    end
                end
                if(cnt24 ==0)
                    S(Id4+nsize) = 2; % Seg(Id3 + 2*nsize);
                else  
                    cnt34 = 0;
                    for kk = 1:CC3.NumObjects
                        Id3 = CC3.PixelIdxList{kk};
                        if(numel(intersect(Id3,Id4))~=0)
                            cnt34 = cnt34 + 1;
                        end
                    end
                    if(cnt34 ==0)
                        S(Id4+2*nsize) = 3; 
                    else
                        S(Id4 + 3*nsize) = 4; % Seg(Id3 + 2*nsize);
                    end
                end
            end     
        end
    end
end