function [I,S] = ImageRd(pth1,pth2,iter)
    I = imread([pth1,num2str(iter),'.png']);
    S = imread([pth2,num2str(iter),'.png']);
end