S11 = im2double(S1);
S11_ = imerode(S11,se);
% figure, imshow(S11 - S11_)
indx = find(abs(S11-S11_)>0);
% tar = I1;
% tar_seg = S1;
I1(indx) = 0;
I1(indx+rw*col) = 0;
I1(indx+2*rw*col) = 0;
S1(indx) = 0;
% figure, subplot 121, imshow(abs(tar - I1)), subplot 122, imshow(abs(tar_seg - S1))

mask1 = (I1(:,:,1)>0 | I1(:,:,2)>0 | I1(:,:,3) > 0);
mask2 = (S1>0);
indx = find(abs(mask1-mask2)>0);
I1(indx) = 0;
I1(indx+rw*col) = 0;
I1(indx+2*rw*col) = 0;
S1(indx) = 0;


mask1 = (I1(:,:,1)>0 | I1(:,:,2)>0|I1(:,:,3)>0);
mask2 = S1>0;
figure, imshow(mask1 - mask2)


nsize = 300*300;
idx = 1:nsize;
L = (zeros(300,300,3));
L(idx) = 235 + fix(2*randn(nsize,1)); % 190
L(idx+nsize) = 240 + fix(2*randn(nsize,1)); % 186
L(idx+2*nsize) = 235 + fix(2*randn(nsize,1)); % 183
mask1 = 1-(S1>0 );
Img1 =  uint8(mask1.*L+double(I1));
figure, imshow(Img1);



path1 = '../SynthDATA/Images/img';
path2 = '../SynthDATA/Segments/seg';

se = strel('disk',3);
rw = 300; col = 300;

I = imread([path1,num2str(iter),'.png']);
S = imread([path2,num2str(iter),'.png']);
I1 = imresize(I,[rw,col],'bicubic');
S1 = imresize(S,[rw,col],'bicubic');
S1(S1>0)=255;
% BW = bwconncomp(S,8);
% for ii = 1:BW.NumObjects
%     if(numel(BW.PixelIdxList{ii}) < 20)
%         S(BW.PixelIdxList{ii}) = 0;
%     end
% end
% 
% S(S>0) = 1;
% figure, imshow(S)


S11 = double(S1);
S11_ = imerode(S11,se);
indx = find(abs(S11-S11_)>0);
I1(indx) = 0;
I1(indx+rw*col) = 0;
I1(indx+2*rw*col) = 0;
S1(indx) = 0;




mask1 = (I(:,:,1)>0 | I(:,:,2)>0 | I(:,:,3)>0);
mask2 = (Sn(:,:,1)>0 | Sn(:,:,2)>0 | Sn(:,:,3)>0);
figure, imshow(abs(mask1 - mask2))


