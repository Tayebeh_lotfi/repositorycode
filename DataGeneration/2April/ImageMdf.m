function [I1,S1] = ImageMdf(pth1,pth2,iter)
    se = strel('disk',3);
    rw = 300; col = 300;

    I = imread([pth1,num2str(iter),'.png']);
    S = imread([pth2,num2str(iter),'.png']);
    I1 = imresize(I,[rw,col],'bicubic');
    S1 = imresize(S,[rw,col],'bicubic');
    S1(S1>0)=255;
    S11 = im2double(S1);
    S11_ = imerode(S11,se);
    indx = find(abs(S11-S11_)>0);
    I1(indx) = 0;
    I1(indx+rw*col) = 0;
    I1(indx+2*rw*col) = 0;
    S1(indx) = 0;

    mask1 = (I1(:,:,1)>0 | I1(:,:,2)>0 | I1(:,:,3) > 0);
    mask2 = (S1>0);
    indx = find(abs(mask1-mask2)>0);
    I1(indx) = 0;
    I1(indx+rw*col) = 0;
    I1(indx+2*rw*col) = 0;
    S1(indx) = 0;
end