clc; close all; clear
pth1 = 'D:\Cytology\modified\';


for index = 3:4
    switch index
        case 2
            I = imread([pth1,'Objects',num2str(index),'.png']);
            S = (I(:,:,1)~=255 & I(:,:,2)~=255 & I(:,:,3)~=255);
            se = strel('disk',10);
            II = imopen(S,se);
%             figure, imshow(II)
            II_ = uint8(II) .* I;
            ind = find(II_(:,:,1)==0 & II_(:,:,2)==0 & II_(:,:,3)==0);
            II_(ind) = 255;
            II_(ind + size(II_,1) * size(II_,2)) = 255;
            II_(ind + 2 * size(II_,1) * size(II_,2)) = 255;
%             figure, imshow(II_)
            imwrite(II_,[pth1,'Objects',num2str(index),'_.png'])
            
            I = imread([pth1,'Objects',num2str(index),'_.png']);
            S = (I(:,:,1)~=255 & I(:,:,2)~=255 & I(:,:,3)~=255);
            figure, imshow(S)
            
            
        case {3,4,5}
            I = im2double(imread([pth1,'Objects',num2str(index),'.png']));
            S = (I(:,:,1)~=1 & I(:,:,2)~=1 & I(:,:,3)~=1);
            se = strel('disk',10);
            II = imclose(S,se);
%             figure, imshow(II,[])
            II_ = repmat(II,[1,1,3]) .* I;
            J = II_(:,:,1)~=1 & II_(:,:,2)~=1 & II_(:,:,3)~=1;
            ind = find(II==1);
            ind_ = find(J==0);

            [tf,loc]=ismember(ind,ind_);
            idx=[1:length(ind)];
            idx=idx(tf);
            idx=idx(loc(tf));
            II_(ind(idx)) = .17;
            II_(ind(idx) + size(II_,1) * size(II_,2)) = .210; II_(ind(idx) + 2*size(II_,1) * size(II_,2)) = .50;
                        
            ind = find(II_(:,:,1)==0 & II_(:,:,2)==0 & II_(:,:,3)==0);
            II_(ind) = 1;
            II_(ind + size(II_,1) * size(II_,2)) = 1;
            II_(ind + 2 * size(II_,1) * size(II_,2)) = 1;
%             J = II_ .* I;
%             figure, imshow(II_)
%             S = (II_(:,:,1)~=1 & II_(:,:,2)~=1 & II_(:,:,3)~=1);
%             S = (J(:,:,1)~=1 & J(:,:,2)~=1 & J(:,:,3)~=1);
%             figure, imshow(S,[])            
            imwrite(II_,[pth1,'Objects',num2str(index),'_.png'])

%             I = imread([pth1,'Objects',num2str(index),'_.png']);
%             S = (I(:,:,1)~=255 & I(:,:,2)~=255 & I(:,:,3)~=255);
%             figure, imshow(S,[])
            
    end
end

I1 = imread([pth1,'Objects1.png']);
S1 = (I1(:,:,1)~=255 & I1(:,:,2)~=255 & I1(:,:,3)~=255);
figure, imshow(S1)




%%
clc; close all; clear
pth1 = 'D:\Cytology\modified\';

J = uint8(zeros(1200,1600,3,5));
JS = zeros(1200,1600,5);
for index = 1:5
    I = imread([pth1,'Objects',num2str(index),'_.png']);
    S = (I(:,:,1)~=255 & I(:,:,2)~=255 & I(:,:,3)~=255);
    J(1:size(I,1),1:size(I,2),1:3,index) = I;
    J(:,size(I,2)+1:end,1,index) = 255;
    J(:,size(I,2)+1:end,2,index) = 255;
    J(:,size(I,2)+1:end,3,index) = 255;
    J(size(I,1)+1:end,:,1,index) = 255;
    J(size(I,1)+1:end,:,2,index) = 255;
    J(size(I,1)+1:end,:,3,index) = 255;
    J(size(I,1)+1:end,size(I,2)+1:end,1,index) = 255;
    J(size(I,1)+1:end,size(I,2)+1:end,2,index) = 255;
    J(size(I,1)+1:end,size(I,2)+1:end,3,index) = 255;
    JS(1:size(S,1),1:size(S,2),index) = S;
end
save('images.mat','J','JS');

%%


clc; close all; clear;
addpath(genpath('../SyntheticData'));
load images.mat;
MaxIter = 2000; % 5000;
height = 20; width = 20; mag = 1; % mag = 3
J(240:410,1174:1316,:,3) = 255; % added
JS(240:410,1174:1316,3) = 0;    % added
J(630:921,347:679,:,3) = 255;
JS(630:921,347:679,3) = 0;
for iter = 1:MaxIter % [25,193,327,438] % 
    Segment = zeros(300,300);
    Image = double(zeros(300,300,3));
    ii = fix(4*rand(1,1))+1;
    Seg = JS(:,:,ii);
    Img = J(:,:,:,ii);
    CC = bwconncomp(Seg,8);
    strtx = 10;
    strty = 10;
    strt = 3;
    for nseg = 1:12 % 8
        JSeg = zeros(1200,1600);
        JImg = uint8(zeros(1200,1600,3));
        jj = fix(CC.NumObjects*rand(1,1))+1;
        idx = CC.PixelIdxList{jj};
        JSeg(idx) = 1;
        JImg(idx) = Img(idx); JImg(idx + numel(JSeg)) = Img(idx + numel(JSeg)); JImg(idx + 2*numel(JSeg)) = Img(idx + 2*numel(JSeg));
        [M,N] = ind2sub([size(JSeg,1),size(JSeg,2)],idx);
        S1 = JSeg(min(M)-strt:max(M)+strt, min(N)-strt: max(N)+strt);
        I1 = im2double(JImg(min(M)-strt:max(M)+strt, min(N)-strt: max(N)+strt,:));
%% added part  
        angle = fix(360*rand(1));
        I1 = imrotate(I1,angle,'bicubic');
        S1 = imrotate(S1,angle,'bicubic');

        if(numel(min(M)-strt:max(M)+strt)>170 || numel(min(N)-strt: max(N)+strt)>170)
            scale = (min(.08,rand(1)/5));
            I1_ = imresize(I1,scale,'bicubic');
            S1_ = imresize(S1,scale,'bicubic');
        end
        if(numel(min(M)-strt:max(M)+strt)>150 || numel(min(N)-strt: max(N)+strt)>150)
            scale = (min(.1,.5-rand(1)/2));
            I1_ = imresize(I1,scale,'bicubic');
            S1_ = imresize(S1,scale,'bicubic');
        end

        if(numel(min(M)-strt:max(M)+strt)>100 || numel(min(N)-strt: max(N)+strt)>100)        
            scale = (min(.3,.5-rand(1)/5));
            I1_ = imresize(I1,scale,'bicubic');
            S1_ = imresize(S1,scale,'bicubic');
        end
        if(numel(min(M)-strt:max(M)+strt)>50 || numel(min(N)-strt: max(N)+strt)>50)
            scale = (min(.4,.5-rand(1)/4));
            I1_ = imresize(I1,scale,'bicubic');
            S1_ = imresize(S1,scale,'bicubic');
        end
        if(numel(min(M)-strt:max(M)+strt)<=50 && numel(min(N)-strt: max(N)+strt)<=50)
            scale = (max(1.2,1+rand(1)/2));
            I1_ = imresize(I1,scale,'bicubic');
            S1_ = imresize(S1,scale,'bicubic');
        end
        
%         B = imwarp(A,tform,'bicubic');
%% End of added part  
        tar = I1_;
        tar_seg = S1_;
        
%         [tar(:,:,1), Tx, Ty] = rBSPwarp( I1_(:,:,1), [height width], mag );
%         [mx my]=ndgrid(1:sz(tar,1), 1:sz(tar,2));
%         tar(:,:,2) =  interp2(my, mx, I1_(:,:,2), my + Ty, mx + Tx, '*linear', 0);      
%         tar(:,:,3) =  interp2(my, mx, I1_(:,:,3), my + Ty, mx + Tx, '*linear', 0);      
%         tar_seg =  interp2(my, mx, S1_, my + Ty, mx + Tx, '*linear', 0);              
% % %         D(:,:,1) = Tx; D(:,:,2) = Ty;
% % %         invD = invertDefField(D);
% % %         invTx = invD(:,:,1); invTy = invD(:,:,2);

% %         Segment(strtx:numel(min(M)-strt:max(M)+strt)+strtx-1,strty:numel(min(N)-strt: max(N)+strt)+strty-1) = tar_seg; % JSeg(min(M)-10:max(M)+10, min(N)-10: max(N)+10);
% %         Image(strtx:numel(min(M)-strt:max(M)+strt)+strtx-1,strty:numel(min(N)-strt: max(N)+strt)+strty-1,:) = tar; % JImg(min(M)-10:max(M)+10, min(N)-10: max(N)+10,:);
        Segment(strtx:size(tar_seg,1)+strtx-1,strty:size(tar_seg,2)+strty-1) = tar_seg; % JSeg(min(M)-10:max(M)+10, min(N)-10: max(N)+10);
        Image(strtx:size(tar,1)+strtx-1,strty:size(tar,2)+strty-1,:) = tar; % JImg(min(M)-10:max(M)+10, min(N)-10: max(N)+10,:);
        strtx = strtx + 110; % 200

        if(strtx > 280)
            strtx = 10;
            strty = strty + 110; % 200
        end
        clear tar; clear tar_seg;
    end
    Im = single(Image);
    imwrite(Im,['SynthDATA_2\Images\img',num2str(iter),'.png']);
    imwrite(Segment,['SynthDATA_2\Segments\seg',num2str(iter),'.png']);
%     imwrite(Im,['SynthDATA\Images\img',num2str(iter),'.png']);
%     imwrite(Segment,['SynthDATA\Segments\seg',num2str(iter),'.png']);
%     figure, subplot 121, imshow(Segment), subplot 122, imshow(Image)
end

%% Editing images
se = strel('disk',2);
rw = 300; col = 300;
for iter = 501:2000
%     I = imread(['SynthDATA/Images/img',num2str(iter),'.png']);
%     S = imread(['SynthDATA/Segments/seg',num2str(iter),'.png']);
    I = imread(['SynthDATA/Images/img',num2str(iter),'.png']);
    S = imread(['SynthDATA/Segments/seg',num2str(iter),'.png']);
    I1 = imerode(I,se);
    S1 = imerode(S,se);
    I1 = imresize(I1,[rw,col],'bicubic');
    S1 = imresize(S1,[rw,col],'bicubic');

    mask1 = (I1(:,:,1)>0 | I1(:,:,2)>0 | I1(:,:,3) > 0);
    mask2 = (S1>0);
    indx = find(abs(mask1-mask2)>0);
    I1(indx) = 0;
    I1(indx+rw*col) = 0;
    I1(indx+2*rw*col) = 0;
    S1(indx) = 0;

    imwrite(I1,['SynthDATA\Modif\Images\img',num2str(iter),'.png']);
    imwrite(S1,['SynthDATA\Modif\Segments\seg',num2str(iter),'.png']);        
%     figure, subplot 121, imshow(I), subplot 122, imshow(I1)
%     figure, subplot 121, imshow(S), subplot 122, imshow(S1)    
end

%% Modifying Images
clear; clc; close all;
addpath(genpath('../../DataGeneration'));
n = 2000;

path1 = '../SynthDATA/Images/img';
path2 = '../SynthDATA/Segments/seg';
for iter = 1:n
%     [I1,S1] = ImageMdf(path1,path2,iter); % Images of size 300*300
    [I1,S1] = ImageMdfv1(path1,path2,iter); % Images of size 200*200
%     imwrite(I1,['../SynthDATA/Modif/Images/img',num2str(iter),'.png']);
%     imwrite(S1,['../SynthDATA/Modif/Segments/seg',num2str(iter),'.png']);
    imwrite(I1,['../SynthDATA/Modifv1/Images/img',num2str(iter),'.png']);
    imwrite(S1,['../SynthDATA/Modifv1/Segments/seg',num2str(iter),'.png']);
end





%% Creating an overlapped data: 350*350 data size
clear; clc; close all;
addpath(genpath('../../DataGeneration'));
n = 2000;
alpha = .55;
beta = .65;
gamma = .55;
len1 = 350;
wid1 = 350;
stp = 20;
nsize = len1*wid1;
idx = 1:nsize;
path1 = '../SynthDATA/Modif/Images/img';
path2 = '../SynthDATA/Modif/Segments/seg';
% path1 = '../SynthDATA/Images/img';
% path2 = '../SynthDATA/Segments/seg';

for iter = 1:100000
    k = 3; % randperm(2,1)+1;
    number = randperm(n,k);
    [I,S] = ReadData(path1,path2,k,len1,wid1,stp,nsize,number,alpha,beta,gamma); % ,rw,col);

    I = resc(I) *255;
    
    L = (zeros(len1,wid1,3));
    L(idx) = 235 + fix(2*randn(nsize,1)); % 190
    L(idx+nsize) = 240 + fix(2*randn(nsize,1)); % 186
    L(idx+2*nsize) = 235 + fix(2*randn(nsize,1)); % 183
    mask1 = 1-(S(:,:,1)>0 | S(:,:,2)>0 | S(:,:,3) >0 );
    G = uint8(mask1.*L+I);
    Sn = postprocss(S,k,nsize);
    imwrite(G,['../SynthDATA/Ovlp_/Images/img',num2str(iter),'.png']);
    niftiwrite(Sn,['../SynthDATA/Ovlp_/Segments/seg',num2str(iter),'.nii']);        
%     imwrite(G,['../SynthDATA\Ovlp_\ImgTst\img',num2str(iter),'.png']);
%     niftiwrite(Sn,['../SynthDATA\Ovlp_\SegTst\seg',num2str(iter),'.nii']);        
end

%% 250*250 data size
clear; clc; close all;
addpath(genpath('../../DataGeneration'));
n = 2000;
alpha = .55;
beta = .65;
gamma = .55;
len1 = 250;
wid1 = 250;
stp = 10; % 20;
nsize = len1*wid1;
idx = 1:nsize;
path1 = '../SynthDATA/Modifv1/Images/img';
path2 = '../SynthDATA/Modifv1/Segments/seg';
% path1 = '../SynthDATA/Images/img';
% path2 = '../SynthDATA/Segments/seg';

for iter = 1:100000
    k = 3; % randperm(2,1)+1;
    number = randperm(n,k);
    [I,S] = ReadData(path1,path2,k,len1,wid1,stp,nsize,number,alpha,beta,gamma); % ,rw,col);

    I = resc(I) *255;
    
    L = (zeros(len1,wid1,3));
    L(idx) = 235 + fix(2*randn(nsize,1)); % 190
    L(idx+nsize) = 240 + fix(2*randn(nsize,1)); % 186
    L(idx+2*nsize) = 235 + fix(2*randn(nsize,1)); % 183
    mask1 = 1-(S(:,:,1)>0 | S(:,:,2)>0 | S(:,:,3) >0 );
    G = uint8(mask1.*L+I);
    Sn = postprocss(S,k,nsize);
    imwrite(G,['../SynthDATA/Ovlpv1_/Images3lyrs/img',num2str(iter),'.png']);
    imwrite(Sn,['../SynthDATA/Ovlpv1_/Segments3lyrs/seg',num2str(iter),'.png']);        
%     imwrite(G,['../SynthDATA\Ovlp_\ImgTst\img',num2str(iter),'.png']);
%     niftiwrite(Sn,['../SynthDATA\Ovlp_\SegTst\seg',num2str(iter),'.nii']);        
end

%% 250*250 data size More overlap (adding 4 layers instead of three)
clear; clc; close all;
addpath(genpath('../../DataGeneration'));
n = 2000;
alpha = .55;
beta = .65;
gamma = .55;
theta = .6;
len1 = 250;
wid1 = 250;
stp = 12;
nsize = len1*wid1;
idx = 1:nsize;
path1 = '../SynthDATA/Modifv1/Images/img';
path2 = '../SynthDATA/Modifv1/Segments/seg';
% path1 = '../SynthDATA/Images/img';
% path2 = '../SynthDATA/Segments/seg';
iter = 1;
while iter <= 10000
    k = 4; % randperm(2,1)+1;
    number = randperm(n,k);
    [I,S] = ReadDatav2(path1,path2,k,len1,wid1,stp,nsize,number,alpha,beta,gamma,theta); % ,rw,col);

    I = resc(I) *255;
    
    L = (zeros(len1,wid1,3));
    L(idx) = 235 + fix(2*randn(nsize,1)); % 190
    L(idx+nsize) = 240 + fix(2*randn(nsize,1)); % 186
    L(idx+2*nsize) = 235 + fix(2*randn(nsize,1)); % 183
    mask1 = 1-(S(:,:,1)>0 | S(:,:,2)>0 | S(:,:,3) >0 | S(:,:,4)>0);
    G = uint8(mask1.*L+I);
    Sn = postprocssv1(S,k,nsize);
    if(numel(find(Sn(:,:,4)>0))==0)
        Sn(:,:,4) = [];
        imwrite(G,['../SynthDATA/Ovlpv1_/Images4lyrs/img',num2str(iter),'.png']);
        imwrite(Sn,['../SynthDATA/Ovlpv1_/Segments4lyrs/seg',num2str(iter),'.png']);        
        iter = iter+1;
%     imwrite(G,['../SynthDATA\Ovlp_\ImgTst\img',num2str(iter),'.png']);
%     niftiwrite(Sn,['../SynthDATA\Ovlp_\SegTst\seg',num2str(iter),'.nii']);   
    end
end

%% 360*480 data size
clear; clc; close all;
addpath(genpath('../../DataGeneration'));
n = 2000;
alpha = .55;
beta = .65;
gamma = .55;
len1 = 360;
wid1 = 480;
stprw = 20;
stpcol = 30;
nsize = len1*wid1;
idx = 1:nsize;
path1 = '../SynthDATA/Modif/Images/img';
path2 = '../SynthDATA/Modif/Segments/seg';
% path1 = '../SynthDATA/Images/img';
% path2 = '../SynthDATA/Segments/seg';

for iter = 1:20000
    k = 3; % randperm(2,1)+1;
    number = randperm(n,k);
    [I,S] = ReadDatav1(path1,path2,k,len1,wid1,stprw,stpcol,nsize,number,alpha,beta,gamma); % ,rw,col);

    I = resc(I) *255;
    
    L = (zeros(len1,wid1,3));
    L(idx) = 235 + fix(2*randn(nsize,1)); % 190
    L(idx+nsize) = 240 + fix(2*randn(nsize,1)); % 186
    L(idx+2*nsize) = 235 + fix(2*randn(nsize,1)); % 183
    mask1 = 1-(S(:,:,1)>0 | S(:,:,2)>0 | S(:,:,3) >0 );
    G = uint8(mask1.*L+I);
    Sn = postprocss(S,k,nsize);
    imwrite(G,['../SynthDATA/Ovlpv1_/ImagesTrns/img',num2str(iter),'.png']);
    imwrite(Sn,['../SynthDATA/Ovlpv1_/SegmentsTrns/seg',num2str(iter),'.png']); 
%     niftiwrite(Sn,['../SynthDATA/Ovlp_/SegmentsTrns/seg',num2str(iter),'.nii']);        
%     imwrite(G,['../SynthDATA\Ovlp_\ImgTst\img',num2str(iter),'.png']);
%     niftiwrite(Sn,['../SynthDATA\Ovlp_\SegTst\seg',num2str(iter),'.nii']);        
end


%% Creating Background
bckgrnd = [190,186,183;228,193,163;254,253,248;150,187,125; 130,170,100;55,73,150];
nsize = 500*500;
idx = 1:nsize;
figure, 
for i = 1:size(bckgrnd,1)
L = uint8(zeros(500,500,3));
L(idx) = bckgrnd(i,1) + fix(randn(nsize,1));
L(idx+nsize) = bckgrnd(i,2) + fix(randn(nsize,1));
L(idx+2*nsize) = bckgrnd(i,3) + fix(randn(nsize,1));
imshow(L)
pause
end





for iter = 1:307
    I = imread(['SynthDATA/Images/img',num2str(iter),'.png']);
    S = imread(['SynthDATA/Segments/seg',num2str(iter),'.png']);
    mask = I>0;
    I1 = imresize(I,[rw,col]);
    S1 = imresize(S,[rw,col]);
    i = fix(size(bckgrnd,1)*rand(1))+1;
    L = uint8(zeros(rw,col,3));
    fix(randn(nsize,1));
    L(idx) = bckgrnd(i,1) + fix(randn(nsize,1));
    L(idx+nsize) = bckgrnd(i,2) + fix(randn(nsize,1));
    L(idx+2*nsize) = bckgrnd(i,3) + fix(randn(nsize,1));
    A = (repmat((255-S1),[1,1,3]) .* L)/255;
    figure, imshow(A,[])
end

%%
path1 = 'C:\Tayebeh\research\Code\DataGeneration\SynthDATA\Ovlpv1_\Images_\img';
path2 = 'C:\Tayebeh\research\Code\DataGeneration\SynthDATA\Ovlpv1_\Segments_\seg';
p1 = 'C:\Tayebeh\research\Code\DataGeneration\SynthDATA\Ovlpv1_\Images\img';
p2 = 'C:\Tayebeh\research\Code\DataGeneration\SynthDATA\Ovlpv1_\Segments\seg';
for ii = 1:50000
    I = imread([path1,num2str(ii),'.png']);
    S = imread([path2,num2str(ii),'.png']);
    imwrite(I,[p1,num2str(ii),'.png']);
    imwrite(S,[p2,num2str(ii),'.png']);
end