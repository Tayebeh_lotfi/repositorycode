clc, close all, clear,
addpath(genpath('../../Code'));
%% generate data

% imgfolder = dir('../../../DataGeneration/SynthDATA/Ovlpv1_/Images/*.png');
% lblfiles = dir('../../../DataGeneration/SynthDATA/Ovlpv1_/Segments/*.png');
% path1 = '../../../DataGeneration/SynthDATA/Ovlpv1_/Images/';
% path2 = '../../../DataGeneration/SynthDATA/Ovlpv1_/Segments/';
imgfolder = dir('../../../DataGeneration/SynthDATA/Ovlpv1_/Images4lyrs/*.png');
lblfiles = dir('../../../DataGeneration/SynthDATA/Ovlpv1_/Segments4lyrs/*.png');
path1 = '../../../DataGeneration/SynthDATA/Ovlpv1_/Images4lyrs/';
path2 = '../../../DataGeneration/SynthDATA/Ovlpv1_/Segments4lyrs/';
thrsh = .9; % 7;
Ntrial = length(imgfolder);
% seg_cnt = zeros(Ntrial,1);
start = 5001;
ed = 5500;
iou = zeros(1,start-ed+1);
acc = zeros(1,start-ed+1);
% load mynet5000Img31Lyrs20Epch % myownnet
load mynet5000Img41Lyrs20Epch
for iter = start:ed
    currentfilename = imgfolder(iter).name;
    X = imread([path1,currentfilename]);
    [~,~,C] = semanticseg(X, net); 
    currentfilename2 = lblfiles(iter).name;
    S = imread([path2,currentfilename2]);
    I = C>thrsh;   
   number = size(find(I==1),1) - size(find(I(:,:,1)==1),1);
   a = size( find(I~=S ) , 1);
   acc(iter-start+1) = 100 - a / numel(I) * 100;
   iou(iter-start+1) = 100 - a / (numel(I) - number) * 100;

end
%     
% 
% %% Compare
%     if(showit==1)
        figure,
        for ii = 1:3
            subplot(3,3,ii), imshow(I(:,:,ii)), if(ii==1), title('Segmentation (Deep Learning)'), end
            subplot(3,4,ii+5), imshow(S(:,:,ii)), if(ii==1), title('Ground Truth'), end
        end
        subplot(3,4,5), imshow(X), title('Mixture')
%     end
