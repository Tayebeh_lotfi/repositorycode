function [imdsTrain, imdsTest, pxdsTrain, pxdsTest] = partitionData_v3(imds,pxds,prtn1,prtn2,varargin)
% Partition CamVid data by randomly selecting 60% of the data for training. The
% rest is used for testing.

% Set initial random state for example reproducibility.
rng(0);
numFiles = numel(imds.Files);
shuffledIndices = randperm(numFiles);

% Use 80% of the images for training.
N = round(prtn1 * numFiles);
trainingIdx = shuffledIndices(1:N);

M = round((prtn1+prtn2) * numFiles);
testIdx = shuffledIndices(N+1:M);

% Use the rest for testing.
% testIdx = shuffledIndices(N+1:end);

% Create image datastores for training and test.
trainingImages = imds.Files(trainingIdx);
testImages = imds.Files(testIdx);
imdsTrain = imageDatastore(trainingImages);
imdsTest = imageDatastore(testImages);

% Extract class and label IDs info.
classes = pxds.ClassNames;
labelIDs = 1:numel(pxds.ClassNames);

% Create pixel label datastores for training and test.
trainingLabels = pxds.Files(trainingIdx);
testLabels = pxds.Files(testIdx);
if(strcmp(varargin{1},'ReadFcn'))
    pxdsTrain = mpixelLabelDatastore(trainingLabels, classes, labelIDs,'ReadFcn',varargin{2});
    pxdsTest = mpixelLabelDatastore(testLabels, classes, labelIDs,'ReadFcn',varargin{2});
end
end