function p = chkDim(p)
% function p = chkDim(p)
%       Ensures p is a M X N array M <= N

[a,b]=size(p);

if a >= b
    p= p';
end
