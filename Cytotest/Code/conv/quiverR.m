function quiverR( arg1, arg2, arg3, arg4, arg5, arg6, arg7)
% function quiverR( dispX, dispY, dispRes, S, C) or quiverR( X, Y, dispX, dispY, dispRes, quiverScale, quiverColor)
%              
%   Quiver with reduced resolution as defined by dispRes (1: same as quiver, 3: 1/3 as many)


if nargin <= 5
    dispX = arg1;
    dispY = arg2;
    dispRes = arg3;
    S = arg4;
    C = arg5;
else
    X = arg1;
    Y = arg2; 
    dispX = arg3;
    dispY = arg4;
    dispRes = arg5;
    S = arg6;
    C = arg7;
end

[n,m]=size(dispX' );
[xi, yi ]= meshgrid(1:n, 1:m);

if ~exist('C','var')
    C = 'k';
end
if ~exist('S','var')
    if min( size(arg1) )==1
        quiver( arg1(1:dispRes:end), arg2(1:dispRes:end), arg3(1:dispRes:end),arg4(1:dispRes:end), C )
    end
    quiver( xi(1:dispRes:end, 1:dispRes:end), yi(1:dispRes:end, 1:dispRes:end), ...
         dispX(1:dispRes:end, 1:dispRes:end), dispY(1:dispRes:end, 1:dispRes:end), C  );
    
else
    if min( size(arg1) )==1
        quiver( arg1(1:dispRes:end), arg2(1:dispRes:end) ,  arg3(1:dispRes:end), arg4(1:dispRes:end) , S, C )
    else
        quiver( xi(1:dispRes:end, 1:dispRes:end), yi(1:dispRes:end, 1:dispRes:end), ...
             dispX(1:dispRes:end, 1:dispRes:end), dispY(1:dispRes:end, 1:dispRes:end), S, C  );
    end
end
axi
axis xy