function [A,A2]= imre( im , dim , SLICE, show, spacing, varagin)
%  function imre( IM, DIM , SLICE, [SPACING] )
% 
%       Views a slice of 3d volume IM at SLICE in dimension DIM.
% 
%       If SPACING is specified, interpolates IM s.t. each voxel becomes
%           approximately isotropic.
% 
% All standard disclaimers apply. Send enquires to lisat@cs.sfu.ca. 


hoff
sz = size( im);
if nargin<=4
   show=1;
spacing = [1 1 1];
end

if length(sz) == 4
    disp('Assume ordering [x y z nfeatures ]')
    switch dim
        case 1
            if SLICE > sz(1), error( 'Requested slice is out of range.'), end;
            A=reshape( im( :,SLICE,:, :), [sz(2) sz(3) sz(4)] );
            sp1=spacing(2);
            sp2=spacing(3);        
        case 2
            if SLICE > sz(2), error( 'Requested slice is out of range.'), end;
            A=reshape( im( :, SLICE, :, :), [sz(1) sz(3) sz(4)] );
            sp1=spacing(1);
            sp2=spacing(3);        
        case 3
            if SLICE > sz(3), error( 'Requested slice is out of range.'), end;
            A= squeeze(im( :, :, SLICE, :),[sz(1) sz(3) sz(4)] );            
            sp1=spacing(1);
            sp2=spacing(2);        
    end
else
    switch dim
        case 1
            if SLICE > sz(1), error( 'Requested slice is out of range.'), end;
            A=reshape( im( SLICE,:, :), [sz(2) sz(3) ] );
            sp1=spacing(2);
            sp2=spacing(3);        
        case 2
            if SLICE > sz(2), error( 'Requested slice is out of range.'), end;
            A=reshape( im( :, SLICE, :), [sz(1) sz(3) ] );
            sp1=spacing(1);
            sp2=spacing(3);        
        case 3
            if SLICE > sz(3), error( 'Requested slice is out of range.'), end;
            A= im( :, :, SLICE) ;
            sp1=spacing(1);
            sp2=spacing(2);        
    end
end
sz = size(A);
 
if ~show
     return;
end
try
    if nargin==5
    [m1n m2n ]=meshgrid(1:1/sp2:size(A,2) ,1:1/sp1:size(A,1) );            
    if exist('varagin', 'var');
    A2=interp2( A, m1n, m2n, varagin);         
    else
    A2=interp2( A, m1n, m2n);         
    end
    imagesc(A2);
else       
    imagesc(A);
    axis image;
    impixelinfo
    end

catch
    return
end