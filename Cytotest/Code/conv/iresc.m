function v1 = iresc(v1 ,  d)
%%%%%%%%%%%%%%%%%%%%%%%%%%
% function v1 = iresc(v1, d)
% 
%       inverse of rescale; d is initial range of v1
%%%%%%%%%%%%%%%%%%%%%%%%%%
 
v1 = double(v1);
 
if d(1) <0
v1 =  v1*diff(d) -  abs( d(1) ) ;
else
v1 =  v1*diff(d) +  abs( d(1) ) ;
end

end