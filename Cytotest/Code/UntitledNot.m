[trainImages,~,trainAngles] = digitTrain4DArrayData;

A = fix(12*rand(30,30,1,200)); B = fix(12*rand(30,30,1,200)); C = A + B;
trainImages = C;
trainAngles = reshape(permute(B,[4,1,2,3]),[200,900]);

numTrainImages = 200;

layers = [ ...
    imageInputLayer([30 30 1])
    convolution2dLayer(12,25)
    reluLayer
    fullyConnectedLayer(900)
    regressionLayer];
options = trainingOptions('sgdm','InitialLearnRate',0.00001, ...
    'MaxEpochs',15);
testnet = trainNetwork(trainImages,trainAngles,layers,options);


AA = fix(12*rand(30,30,1)); BB = fix(12*rand(30,30,1));
testImg = AA + BB; testAngl = BB;

predictedTestAngles = predict(testnet,testImg);
pred = reshape(predictedTestAngles,[30,30]);


