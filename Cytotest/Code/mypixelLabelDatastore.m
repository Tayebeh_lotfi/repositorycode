function pxds = mypixelLabelDatastore(labelDir,classes,labelIDs, varargin)
    if nargin<4
        for ii = 1:700
            pxds.Files{ii} = [pwd,'\',labelDir,'Lbl',num2str(ii),'.nii'];
        end  
    else
        pxds.Files = labelDir;
    end
    pxds.ClassNames = cellstr(classes);
end